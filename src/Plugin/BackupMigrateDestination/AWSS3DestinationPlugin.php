<?php

namespace Drupal\backup_migrate_aws_s3\Plugin\BackupMigrateDestination;

use Drupal\backup_migrate\Drupal\EntityPlugins\DestinationPluginBase;
/**
 * Defines a file directory destination plugin.
 *
 * @BackupMigrateDestinationPlugin(
 *   id = "AWSS3",
 *   title = @Translation("Amazon S3"),
 *   description = @Translation("Back up to a Amazon S3."),
 *   wrapped_class = "Drupal\backup_migrate_aws_s3\Destination\DrupalAWSS3Destination"
 * )
 */
class AWSS3DestinationPlugin extends DestinationPluginBase {
}

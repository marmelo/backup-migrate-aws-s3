<?php

namespace Drupal\backup_migrate_aws_s3\Entity;

use Drupal\backup_migrate_aws_s3\MyentityInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the advertiser entity.
 *
 * @ingroup myentity
 *
 * @ConfigEntityType(
 *   id = "myentity",
 *   label = @Translation("My Entity"),
 *   handlers = {
 *     "list_builder" = "Drupal\backup_migrate_aws_s3\Controller\MyentityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\backup_migrate_aws_s3\Form\MyEntityForm",
 *     }
 *   },
 *   config_prefix = "myentity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   }
 * )
 */

class MyEntity extends ConfigEntityBase implements MyentityInterface {

  public $id;
  public $text;
}
